package com.timwiconsulting.codingchallenge.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.timwiconsulting.codingchallenge.service.ElvesService;

/**
 * Elves Rest Controller
 * @author p.dapa
 *
 */
@RestController
@RequestMapping("elves")
public class ElvesController {
	
	/**
	 * Class logger
	 */
    private static final Logger LOGGER = LogManager.getLogger(ElvesController.class);
    
	@Autowired
	private ElvesService elvesService;
	
	/**
	 * Read Digits
	 * @param digits digits
	 * @param numberOfRepeats number of repeats
	 * @return digits
	 */
	@GetMapping(value = "/read/digits/{digits}/numberOfRepeats/{numberOfRepeats}", 
		consumes = {"text/plain;charset=UTF-8", MediaType.ALL_VALUE}, 
		produces = {"text/plain;charset=UTF-8", MediaType.APPLICATION_JSON_VALUE})
	public String readDigits(@PathVariable int digits, @PathVariable int numberOfRepeats) {
		Map<String, String> response = new HashMap<>();
		String retour = this.elvesService.readDigits(digits, numberOfRepeats);
		response.put("digits : "+ String.valueOf(digits) + " ", " read of digits : " + retour);
		LOGGER.info("digits = " + digits + ", read of digits = " + retour + ", number of repeats = " + numberOfRepeats);
		return response.toString().replace("=", ",");
	}
}
