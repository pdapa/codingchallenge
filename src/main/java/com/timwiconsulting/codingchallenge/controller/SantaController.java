package com.timwiconsulting.codingchallenge.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.timwiconsulting.codingchallenge.service.SantaService;
import com.timwiconsulting.codingchallenge.utils.FileService;

/**
 * Santa Rest Controller
 * @author p.dapa
 *
 */
@RestController
@RequestMapping("santa")
public class SantaController {
	
	/**
	 * Class logger
	 */
    private static final Logger LOGGER = LogManager.getLogger(SantaController.class);
    
	@Autowired
	private SantaService santaService;
	
	/**
	 * Default method
	 * @return web page (html)
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/", 
		produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView welcome() {
		ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName("index");
	    return modelAndView;
	}
	
	/**
	 * Read Digits
	 * @param text text
	 * @return digits
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/instructionsFloor/text/{text}", 
		consumes = {"text/plain;charset=UTF-8", MediaType.ALL_VALUE}, 
		produces = {"text/plain;charset=UTF-8", MediaType.APPLICATION_JSON_VALUE})
	public String instructionsFloor(@PathVariable String text) {
		Map<String, String> response = new HashMap<>();
		
		String instructionsFloor = this.santaService.instructionsFloor(text);
		response.put("Resulting Floor ", instructionsFloor);
		LOGGER.info("Resulting Floor " + instructionsFloor);
		return response.toString().replace("=", ",");
	}
	
	/**
	 * Return instructions floor
	 * @param file file
	 * @return instructions floor
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/instructionsFloor/file", 
		consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, 
		produces = {"text/plain;charset=UTF-8", MediaType.APPLICATION_JSON_VALUE})
	public String instructionsFloor(@RequestParam("file") MultipartFile file) {
		Map<String, String> response = new HashMap<>();
		
		String fileContent = "";
		String retour = "";
		Optional<String> fileExtension = FileService.getExtensionByStringHandling(file.getName());
		if (!fileExtension.isEmpty() && fileExtension.toString().equalsIgnoreCase("txt")) {

			try {
				fileContent = FileService.readFromInputStream(file.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				retour = "Erreur lors du chargement du fichier : Fichier corrompu!";
				return retour;
			}
			String intructionsFloor = this.santaService.instructionsFloor(fileContent);
			response.put("Resulting Floor ", intructionsFloor);
			LOGGER.info("Resulting Floor " + intructionsFloor);
			retour = response.toString().replace("=", ",");
		} else {
			retour = "Erreur lors du chargement du fichier TXT : Mauvaise Extension détectée!";
		}
		return retour;
	}
}
