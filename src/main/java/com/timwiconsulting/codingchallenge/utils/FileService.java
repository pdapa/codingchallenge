package com.timwiconsulting.codingchallenge.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

import org.springframework.stereotype.Service;

/**
 * File service
 * @author p.dapa
 *
 */
@Service
public class FileService {

	/**
	 * Read file content to string
	 * @param inputStream input stream
	 * @return file content
	 * @throws IOException exception thrown
	 */
	public static String readFromInputStream(InputStream inputStream) throws IOException {
		StringBuilder resultStringBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				resultStringBuilder.append(line).append("\n");
			}
		}
		return resultStringBuilder.toString();
	}
	
	/**
	 * Return file extension
	 * @param filename file name
	 * @return file extension
	 */
	public static Optional<String> getExtensionByStringHandling(String filename) {
	    return Optional.ofNullable(filename)
	      .filter(f -> f.contains("."))
	      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}
}
