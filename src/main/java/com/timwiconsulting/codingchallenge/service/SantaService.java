package com.timwiconsulting.codingchallenge.service;

import org.springframework.stereotype.Service;

/**
 * 
 * @author p.dapa
 * Elves Service 
 *
 */
@Service
public class SantaService {

	/**
	 * Return a floor number<br>
	 * regarding to a stream
	 * @param digits digits
	 * @param numberOfRepeats number of repeats
	 * @return a list of "digits"
	 */
	public String instructionsFloor(String stream) {
		if (stream != null && !stream.isEmpty()) {
			long countOpeningParenthesis = this.numberOfOccurences(stream, '(');
			long countClosingParenthesis = this.numberOfOccurences(stream, ')');
			return String.valueOf(countOpeningParenthesis - countClosingParenthesis);
		} else {
			return "0";
		}
	}
	
	private long numberOfOccurences(String stream, char c) {
		return stream.chars().filter(ch -> ch == c).count();
	}
}
