package com.timwiconsulting.codingchallenge.service;

import org.springframework.stereotype.Service;

/**
 * 
 * @author p.dapa
 * Elves Service 
 *
 */
@Service
public class ElvesService {

	/**
	 * Return a list of "digits" involving<br>
	 * each digit contained in the inputs digit<br>
	 * followed by the number of times it appears<br>
	 * consecutively.
	 * @param digits digits
	 * @param numberOfRepeats number of repeats
	 * @return a list of "digits"
	 */
	public String readDigits(int digits, int numberOfRepeats) {
		StringBuilder retour = new StringBuilder("");
		for (int i = 0; i < numberOfRepeats; i++) {
			String chaine = String.valueOf(digits);
			int j = 0;
			while (j < chaine.length()) {
				char currentDigit = chaine.charAt(j);
				int numberOfOccurrences = this.numberOfConsecutiveDigits(chaine, currentDigit, j);
				retour.append(numberOfOccurrences + "" + currentDigit);
				j = j + numberOfOccurrences;
			}
		}
		return retour.toString();
	}
	
	private int numberOfConsecutiveDigits(String chaine, char digit, int start) {
		int k = start;
		int numberOfOccurrences = 0;
		while (k < chaine.length()) {
			if (chaine.charAt(k) == digit) {
				numberOfOccurrences++;
			} else {
				break;
			}
			k++;
		}
		return numberOfOccurrences;
	}

}
