package com.timwiconsulting.codingchallenge.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.timwiconsulting.codingchallenge.service.ElvesService;

public class ElvesControllerTest {
	
	@InjectMocks
	private ElvesController elvesController;
	
	@Mock
	private ElvesService elvesService;
	

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

	/**
	 * Method test of {@link ElvesController#readDigits(long, long)}
	 * @return
	 */
	@Test
	public void testReadDigits() {
		// GIVEN
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(1, 1)).thenReturn("11");
        // THEN with 1 repeat
        assertEquals("{digits : 1 , read of digits : 11}",
        		this.elvesController.readDigits(1, 1));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(11, 1)).thenReturn("21");
        // THEN with 1 repeat
        assertEquals("{digits : 11 , read of digits : 21}",
        		this.elvesController.readDigits(11, 1));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(211, 1)).thenReturn("1221");
        // THEN with 1 repeat
        assertEquals("{digits : 211 , read of digits : 1221}",
        		this.elvesController.readDigits(211, 1));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(1211, 1)).thenReturn("111221");
        // THEN with 1 repeat
        assertEquals("{digits : 1211 , read of digits : 111221}",
        		this.elvesController.readDigits(1211, 1));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(111221, 1)).thenReturn("312211");
        // THEN with 1 repeat
        assertEquals("{digits : 111221 , read of digits : 312211}",
        		this.elvesController.readDigits(111221, 1));
        // WHEN with 1 repeat
        Mockito.when(this.elvesService.readDigits(1113122113, 1)).thenReturn("311311222113");
        // THEN with 1 repeat
        assertEquals("{digits : 1113122113 , read of digits : 311311222113}",
        		this.elvesController.readDigits(1113122113, 1));
        
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(1, 2)).thenReturn("1111");
        // THEN with 2 repeat
        assertEquals("{digits : 1 , read of digits : 1111}",
        		this.elvesController.readDigits(1, 2));
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(11, 2)).thenReturn("2121");
        // THEN with 2 repeat
        assertEquals("{digits : 11 , read of digits : 2121}",
        		this.elvesController.readDigits(11, 2));
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(211, 2)).thenReturn("12211221");
        // THEN with 2 repeat
        assertEquals("{digits : 211 , read of digits : 12211221}",
        		this.elvesController.readDigits(211, 2));
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(1211, 2)).thenReturn("111221111221");
        // THEN with 2 repeat
        assertEquals("{digits : 1211 , read of digits : 111221111221}",
        		this.elvesController.readDigits(1211, 2));
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(111221, 2)).thenReturn("312211312211");
        // THEN with 2 repeat
        assertEquals("{digits : 111221 , read of digits : 312211312211}",
        		this.elvesController.readDigits(111221, 2));
        // WHEN with 2 repeat
        Mockito.when(this.elvesService.readDigits(1113122113, 2)).thenReturn("311311222113311311222113");
        // THEN with 2 repeat
        assertEquals("{digits : 1113122113 , read of digits : 311311222113311311222113}",
        		this.elvesController.readDigits(1113122113, 2));
	}

}
